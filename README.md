
## Prerequisites

You must have npm installed on your system in order to run the project.

## Getting started

- run ``npm install`` inside the project directory
- run ``npm run dev`` to start the dev server (default port is 5173)

## Task

The project has been set up as a template for you to contain everything you should need. Dependencies already include **Vue**, **Pinia** for store management, **axios** as HTTP client and **D3.js** as a charting library. The task will consist of calling an API Endpoint to fetch some data and displaying the data on a page including basic charts.

As far as the Vue API goes, you're free to use either Composition or Options API. Using other npm packages/libraries and extending the project configurations is allowed of course if you want or feel the need for it. For the implementation you don't have to care about browser compatability as long as it runs on the latest version of modern browsers. Other than the requirements in the list below, the task is quite open - just try to stick to best practices and implement a solution you're satisfied with.

For the submission, clone the repository and send us your solution as a zipped folder **via E-Mail** to rjuettner@behaviorquant.com. The folder should not be bigger than ~5MB (so please leave out dependencies, it should work by running the two npm commands above).

If you run into any problems or have questions feel free to contact us anytime (rjuettner@behaviorquant.com or bbujtor@behaviorquant.com).

# Requirements

1. Call an API Endpoint (GET) defined as ``dummyEndpoint`` in ``src/constants/index.js``. This endpoint will return a tree-like object of items in this format:

```
{
    "itemId": 1,
    "name": "ROOT",
    "result": 37,
    "children": [
        {
            "itemId": 2,
            "name": "ITEM_A",
            "result": 13,
            "children": [...]
        },
        {
            "itemId": 3,
            "name": "ITEM_B",
            "result": 23,
            "children": [...]
        }
    ]
}
```

2. Implement a page which will display the value ``result`` of all the **leaf-nodes** of the response object in a single respective chart for each value. The charts should all be displayed on the same page. The chart should either be a donut or a pie chart while you can assume the ``result`` value to be a percentage. Each chart should be labeled with the corresponding items ``name``.  
Example:  
![image example](./public/example-donut-1.png)

3. The respective charts should have a fixed size (around 200-400px) and should be displayed next to each other in a row while **not overflowing** the screen horizontally and perform **wrapping** if needed, i.e. if the screen size is getting smaller.

4. The component should also display the **average value** of all the leaf-nodes somewhere on the page.

5. While the request is pending, show a **loading indicator** of any kind on the page.

6. Additional Task (depending on how long you need): Implement a second separate page/view which can be navigated to or toggled. It should display the **ROOT** item and its direct/immediate children the same way the leaf items are displayed.

6. If you want, try to make the page and layout more appealing by using some additional CSS. 💅 

7. The solution does not have to perfect - it should primarily show how you approached the task, what you can do and give us a basis for talking about it. 🙂