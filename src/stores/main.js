import { defineStore } from 'pinia'

export const useMainStore = defineStore('main-store', {
  state: () => {
    return {}
  },
  getters: {
  },
  actions: {
  }
})
