/**
 * This is the endpoint providing the dummy data to you.
 * Method: GET
 */
export const dummyEndpoint = 'https://mocki.io/v1/5da32c5a-4fd5-44b8-a8db-2ad48e75327c'